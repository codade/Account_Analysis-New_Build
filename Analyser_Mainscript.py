
'''This is the main script started by the user. After asking whether data shall be processed or just plotted the data is categorized and preprocessed for plotting.
If data is just read in for plotting the user can choose between the different types of plots. For raw data from csv-files beeing processed the plotting choice is predetermined.
After preprocessing the data is saved as excel files in the same folder as the plots. Plot data-format is *.jpg. Have fun using the script'''


from Base_Functions import data_processor
from Base_Functions import selector
from Base_Functions import plot_preprocessor


print('\nImport der benötigten Module erfolgreich!\n\n')

import numpy as np
import pandas as pd
import datetime
import locale
import re

folder_dir=input('Bitte geben Sie den genauen Ordnerpfad an, in dem sich alle Rohdaten für die Auswertung befinden. In diesem Ordner werden auch die Ergebnisse abgelegt.')+'/'

accounts_data=data_processor.Accounts_Data(folder_dir)

##Ask if data shall be processed or just plotted
main_select=selector.main_selector()
if main_select=='single':
    ##Get csv-files
    continue_csv='j'
    while continue_csv=='j':

        while True:
            try:
                acc_type=selector.acc_selector()
                filename=input('Bitte geben Sie den genauen Dateinamen ohne Dateiendung ein!\n')
                accounts_data.process_data(filename,acc_type)
            except IOError:
                print('\nDie eingegebene Datei konnte nicht gefunden werden. Versuchen Sie es bitte nocheinmal!')
                continue
            except IndexError:
                print('\nEs gab ein Problem beim Einlesen der Datei; der ausgewählte Kontotyp passt nicht zur angegebenen Datei. Versuchen Sie es bitte nocheinmal!')
                continue
            except ValueError:
                print('\nEs gab ein Problem beim Einlesen der Datei; der ausgewählte Kontotyp passt nicht zur angegebenen Datei. Versuchen Sie es bitte nocheinmal!')
                continue
            else:
                break
        continue_csv=selector.yes_no_selector('Sollen weitere Daten eingelesen werden?\n')
    
    print('\nImport der csv-Dateien erfolgreich abgeschlossen!\n\n')
    #concat holiday data
    holiday=selector.yes_no_selector('Sollen Umsätze der Kategorie Urlaub in einem eigenen Datensatz zusammengefasst und ausgewertet werden?\n')
    if holiday=='j':
        accounts_data.bundle_holiday()

    else:#no action needed
        pass
        
    #concat data if selected
    concatting_term=selector.yes_no_selector('\nMöchten Sie zusätzlich einzelne der importierten csv-Datensätze zu einem neuen Datensatz zusammenfassen und diesen ebenfalls auswerten?\n')
    if concatting_term=='j':
        inserter=''
        concat_list_term=accounts_data.concat_selector('csv-concat',inserter)
        accounts_data.concatter(concat_list_term)
    else:#no action needed
        pass

    #concat data with exitsting excel-file
    concatting_longterm=selector.yes_no_selector('\nMöchten Sie zusätzlich einzelne der importierten csv-Datensätze zu einer bestehenden Langzeitanalyse (Excel-Datei) hinzufügen und diese auswerten?\n')
    if concatting_longterm=='j':
        longterm_counter=0
        while concatting_longterm=='j':
            while True:
                try:
                    print('\nBitte beachten Sie: Die Excel-Datei, zu der Sie csv-Daten hinzufügen möchten, muss ein Tabellenblatt "Aufbereitete Daten" mit entsprechender Formatierung bisheriger Auswertungen haben!')
                    filename_excel=input('\nBitte geben Sie den genauen Namen der Excel-Datei ein!\n' )
                    accounts_data.process_data(filename_excel,'excel_concat')
                except :
                    print('Die eingegebene Datei konnte nicht eingelesen werden. Versuchen Sie es bitte nocheinmal!')
                    continue
                else:
                    break
            counter_list=accounts_data.concat_selector('xlsx-concat',filename_excel)
            if longterm_counter==0:
                concat_list_longterm=counter_list.copy()
            else:
                concat_list_longterm.append(counter_list[0])
            concatting_longterm=selector.yes_no_selector('Möchten Sie Daten zu einer weiteren Excel-Datei hinzufügen?')
            longterm_counter+=1
        accounts_data.concatter(concat_list_longterm)

    else:#no action needed
        pass

    print('\nDie Datenaufbereitung ist jetzt erfolgreich abgeschlossen.\n\n')
    
#just plot data starts here
else:
    continue_plotdata='j'
    while continue_plotdata=='j':

        while True:
            try:
                print('\nBitte beachten Sie: Die Excel-Datei, für die Sie Plots erstellen möchten, muss ein Tabellenblatt "Aufbereitete Daten" mit entsprechender Formatierung bisheriger Auswertungen haben!')
                filename_excel=input('Bitte geben Sie den genauen Namen der Excel-Datei ein!')
                accounts_data.process_data(filename_excel,'excel_plot')
            except :
                print('Die eingegebene Datei konnte nicht eingelesen werden. Versuchen Sie es bitte nocheinmal!')
                continue
            else:
                break
        accounts_data.plotting_list[filename_excel]=selector.plot_selector(accounts_data)
        continue_plotdata=selector.yes_no_selector('\nMöchten Sie Plots zu weiteren Dateien erstellen?\n')
        
#make categorical sorting
accounts_data.month_cat_maker()
#create folders and excel output files
accounts_data.makefolder_excel()
print('Die Plots werden nun erstellt. Bitte haben Sie einen Moment Geduld!\n\n')

##Start plotting section
plot_data=plot_preprocessor.Plotters(accounts_data)
#Preprocess Plotting Data
plot_data.makeplotdata()
#Create Plots and save 'jpgs'
plot_data.plotdata()

print('\nAlles fertig! Das Programm kann jetzt geschlossen werden.\n')