
'''This file holds all relevant functions necessary for starting the data analysis.
An object class for all account data is established, which will hold the raw data after import,
the processed data and all subdata configuration necessary for plotting. If desired credit data is also integrated. 
Currently it is only working for comdirect bank data. Further work towards integrating other banks will be done. Excel file is exported at the end exported.'''


from Base_Functions import classifier
import numpy as np
import pandas as pd
import datetime
import locale
import os
import re

locale.setlocale(locale.LC_ALL, 'de_DE.utf8')
mydateparser = lambda x: datetime.datetime.strptime(x, "%d.%m.%Y")
mydateparser_mlp= lambda x: datetime.datetime.strptime(x, "%d.%m.%y")



class Accounts_Data:

    def __init__(self,folder_raw):

        self.imported_csv=[]
        self.raw_data={}
        self.raw_data_header={}
        self.basis_data={}
        self.month_data={}
        self.summe_rest={}
        self.cat_data={}
        self.plotting_list={}
        self.folder_raw=folder_raw
        self.folder_res={}

    def process_data(self,filename,acc_type):

        ##read in csv-files from different account_types
        
        if acc_type=="comd_giro":
            
            #read csv
            dataname=self.folder_raw+filename+'.csv'
            self.raw_data[filename]=pd.read_csv(dataname,sep=";",encoding="iso8859_15",skiprows=4,index_col=5,thousands='.',decimal=',',skipfooter=2,engine='python',parse_dates=[0,1],date_parser=mydateparser,).reset_index(drop=True)
            self.raw_data[filename].columns=["time1","time2","act","text","val"]
            self.raw_data_header[filename]=["Buchungstag","Wertstellung (Valuta)","Vorgang","Buchungstext","Umsatz in EUR"]
            self.basis_data[filename]=self.raw_data[filename].copy()
            self.basis_data[filename]['month']=self.basis_data[filename]['time1'].apply(lambda dates: dates.strftime('%b/%Y'))
            self.basis_data[filename]=classifier.categorize_data('giro',self.basis_data[filename])
            self.basis_data[filename].loc[self.basis_data[filename]['cat']=='Bargeld','cat']='Bargeld\nComdirect'
            
            #add info for subsequent handling
            self.plotting_list[filename]=['a','o']
            self.imported_csv.append(filename)
            self.folder_res[filename]=self.folder_raw+'Ergebnisse/'+filename
            
        elif acc_type=="comd_credit":
            
            #read csv
            dataname=self.folder_raw+filename+'.csv'
            self.raw_data[filename]=pd.read_csv(dataname,sep=";",encoding="iso8859_15",skiprows=4,index_col=6,thousands='.',decimal=',',skipfooter=2,engine='python',parse_dates=[0,1],date_parser=mydateparser).reset_index(drop=True)
            self.raw_data[filename].columns=["time1","time2","act","ref","text","val"]
            self.raw_data_header[filename]=["Buchungstag","Wertstellung (Valuta)","Vorgang","Referenz","Buchungstext","Umsatz in EUR"]
            self.basis_data[filename]=self.raw_data[filename].copy()
            self.basis_data[filename]['ref']=self.basis_data[filename][self.basis_data[filename].columns[3]].apply(lambda item: "{}{}".format('Ref. ', item))
            self.basis_data[filename]['text'] = self.basis_data[filename][self.basis_data[filename].columns[3:5]].apply(lambda x: ' / '.join(x.dropna()),axis=1)
            self.basis_data[filename].drop("ref",axis=1,inplace=True)
            self.basis_data[filename]['month']=self.basis_data[filename]['time1'].apply(lambda dates: dates.strftime('%b/%Y'))
            self.basis_data[filename]=classifier.categorize_data('giro',self.basis_data[filename])
            
            #add info for subsequent handling
            self.plotting_list[filename]=['c','m','v']
            self.imported_csv.append(filename)
            self.folder_res[filename]=self.folder_raw+'Ergebnisse/'+filename

        elif acc_type=="dkb_giro":
            
            #read csv            
            dataname=self.folder_raw+filename+'.csv'
            self.raw_data[filename]=pd.read_csv(dataname,sep=";",encoding="iso8859_15",skiprows=6,index_col=11,thousands='.',decimal=',',engine='python',parse_dates=[0,1],date_parser=mydateparser).reset_index(drop=True)
            self.raw_data_header[filename]=['Buchungstag', 'Wertstellung', 'Buchungstext','Auftraggeber / Begünstigter', 'Verwendungszweck', 'Kontonummer', 'BLZ', 'Betrag (EUR)','Gläubiger-ID','Mandatsreferenz','Kundenreferenz']
            self.basis_data[filename]=self.raw_data[filename].copy()
            self.basis_data[filename].drop(self.basis_data[filename].columns[[8,9,10]],axis=1,inplace=True)
            self.basis_data[filename]['text'] = self.basis_data[filename][self.basis_data[filename].columns[3:6]].apply(lambda x: ' / '.join(x.dropna()),axis=1)
            self.basis_data[filename].drop(self.basis_data[filename].columns[[3,4,5,6]],axis=1,inplace=True)
            self.basis_data[filename]=self.basis_data[filename].reindex(columns=self.basis_data[filename].columns[[0,1,2,4,3]])
            self.basis_data[filename].columns=["time1","time2","act","text","val"]
            self.basis_data[filename]['month']=self.basis_data[filename]['time1'].apply(lambda dates: dates.strftime('%b/%Y'))
            self.basis_data[filename]=classifier.categorize_data('giro',self.basis_data[filename])
            self.basis_data[filename].loc[self.basis_data[filename]['cat']=='Kosten Lebens-\nhaltung','cat']='Einzahlungen'
            
            #add info for subsequent handling
            self.plotting_list[filename]=['a','o']
            self.imported_csv.append(filename)
            self.folder_res[filename]=self.folder_raw+'Ergebnisse/'+filename
            
        elif acc_type=="dkb_credit":
            
            #read csv
            dataname=self.folder_raw+filename+'.csv'
            self.raw_data[filename]=pd.read_csv(dataname,sep=";",encoding="iso8859_15",skiprows=6,index_col=6,thousands='.',decimal=',',engine='python',parse_dates=[1,2],date_parser=mydateparser).reset_index(drop=True)
            self.raw_data[filename]=self.raw_data[filename].reindex(columns=self.raw_data[filename].columns[[2,1,3,4,5,0]])
            self.raw_data_header[filename]=["Wertstellung","Belegdatum","Beschreibung","Betrag (EUR)","Ursprünglicher Betrag","Umsatz abgerechnet und nicht im Saldo enthalten"]
            self.basis_data[filename]=self.raw_data[filename].copy()
            self.basis_data[filename].drop(self.basis_data[filename].columns[[4,5]],axis=1,inplace=True)
            self.basis_data[filename].insert(2,'act',pd.Series())
            self.basis_data[filename].columns=["time1","time2","act","text","val"]
            self.basis_data[filename]['month']=self.basis_data[filename]['time1'].apply(lambda dates: dates.strftime('%b/%Y'))
            self.basis_data[filename]=classifier.categorize_data('credit',self.basis_data[filename])
            
            #add info for subsequent handling
            self.plotting_list[filename]=['c','m','v']
            self.imported_csv.append(filename)
            self.folder_res[filename]=self.folder_raw+'Ergebnisse/'+filename


        elif acc_type=='excel_concat':
            
            #read excel
            dataname=self.folder_raw+filename+'.xlsx'
            self.basis_data[filename]=pd.read_excel(dataname,sheet_name='Aufbereitete Daten')
            self.basis_data[filename].columns=["time1","time2","act","text","val",'month','cat']

        elif acc_type=='excel_plot':
            
            #read excel
            dataname=self.folder_raw+filename+'.xlsx'
            self.basis_data[filename]=pd.read_excel(dataname,sheet_name='Aufbereitete Daten')
            self.basis_data[filename].columns=["time1","time2","act","text","val",'month','cat']
            
            #add info for subsequent handling
            self.folder_res[filename]=self.folder_raw+'Ergebnisse/'+filename

        else:##MLP&Triodos
            
            #read csv
            self.plotting_list[filename]=['a','o']
            self.imported_csv.append(filename)
            self.folder_res[filename]=self.folder_raw+'Ergebnisse/'+filename
            dataname=self.folder_raw+filename+'.csv'
            self.raw_data[filename]=pd.read_csv(dataname,sep=";",encoding="iso8859_15",skiprows=12,skipfooter=3,thousands='.',decimal=',',engine='python',parse_dates=[0,1],date_parser=mydateparser_mlp).reset_index(drop=True)
            self.raw_data_header[filename]=["Buchungstag","Valuta","Auftraggeber/Zahlungsempfänger","Empfänger/Zahlungspflichtiger","Konto-Nr.","IBAN","BLZ","BIC","Vorgang/Verwendungszweck","Kundenreferenz","Währung","Umsatz"]
            self.basis_data[filename]['text'] = self.basis_data[filename][self.basis_data[filename].columns[[3,8,9]]].apply(lambda x: ' / '.join(x.dropna()),axis=1)
            self.basis_data[filename].drop(self.basis_data[filename].columns[[2,3,4,5,6,7,8,9,10]],axis=1,inplace=True)
            self.basis_data[filename]=self.basis_data[filename].reindex(columns=self.basis_data[filename].columns[[0,1,3,4,2]])
            self.basis_data[filename].columns=["time1","time2","act","text","val"]
            self.basis_data[filename]['month']=self.basis_data[filename]['time1'].apply(lambda dates: dates.strftime('%b/%Y'))
            self.basis_data[filename]=classifier.categorize_data('giro',self.basis_data[filename])
            
            #add info for subsequent handling
            self.plotting_list[filename]=['a','o']
            self.imported_csv.append(filename)
            self.folder_res[filename]=self.folder_raw+'Ergebnisse/'+filename
         
        
    def concat_selector(self,concat_type,inserter):
        
        ##needed if user wishes to concat specific data 

        folder_list=list(self.folder_res.keys())
        print('\nSie können folgende Daten zu einer zusammengefassten Datei hinzufügen:', end=' ')
        print(*[(f'{txt} [{pos}]') for pos,txt in list(enumerate(folder_list))],sep=', ')  
        while True:
            #ask which data to concat
            try:
                if concat_type=='csv-concat':
                    concat_selection=input('\nBitte geben Sie an, welche Daten Sie zusammen auswerten möchten.\nNutzen Sie folgende Eingabemethode (z.B. 0,3;1,2), wenn Sie zum Beispiel die Daten mit Nummer 0 und 3 sowie Nummer 1 und 2 zusammen auswerten möchten.')
                else:
                    concat_selection=input('\nBitte geben Sie an, welche Daten Sie zu der Excel-Datei hinzufügen möchten.\nNutzen Sie folgende Eingabemethode (z.B. 0,1), wenn Sie zum Beispiel die Daten mit Nummer 0 und 1 zu den bestehenden Excel-Daten hinzufügen möchten.')
                concat_list=[list(concat_selection.split(';'))[entry].split(',') for entry in range(0,len(list(concat_selection.split(';'))))]
                mainlist=[]
                for entry in range(0,len(concat_list)):
                    mysublist=[]
                    for sub in range(0,len(concat_list[entry])):
                        mysublist.append(self.basis_data[folder_list[int(concat_list[entry][sub])]])
                    mainlist.append(mysublist)
            except IndexError:
                print('Sie haben eine Zahl eingeben, zu der keine Daten vorlegen. Sie können nur zwischen den bereits eingelesenen Daten wählen!')
                continue
            except ValueError:
                print('Bitte geben Sie eine gültige Zahlenkombination im Format des Bespiels ein! Buchstaben sind nicht zulässig.')
            else:
                break

        if not inserter=='':
            mainlist[0].insert(0,self.basis_data[inserter])

        else:#no action needed
            pass
 
        return mainlist
        
    def concatter(self,concat_list):
        
        #create list to find concat choice in datasets
        
        for item in range(0,len(concat_list)):
            #get names for new datasets
            if len(concat_list)>1:
                framename=input(f'\nBitte geben Sie einen Namen für den {item+1}. zu verbindenden Datensatz ein.\nDer Name muss von den bisher verwendeten (Datei-)Namen abweichen\n')
            else:
                framename=input('\nBitte geben Sie einen Namen für den zu verbindenden Datensatz ein.\nDer Name muss von den bisher verwendeten (Datei-)Namen abweichen\n')

            #concat and add to data-object
            self.basis_data[framename]=pd.concat(concat_list[item],ignore_index=True)
            self.basis_data[framename]=self.basis_data[framename].sort_values(['time1'],ascending=False) 
            self.folder_res[framename]=self.folder_raw+'Ergebnisse/'+framename
            self.plotting_list[framename]=['a','o']
    
    
    def bundle_holiday(self):
        #concat holiday data from different csv-files
        
        self.basis_data['Urlaube']=pd.DataFrame(columns=["time1","time2","act","text","val","month","cat"])
        for entry in range(0,len(self.imported_csv)):
            data_hol=self.basis_data[self.imported_csv[entry]].loc[self.basis_data[self.imported_csv[entry]]['cat'].str.contains('Urlaub')].copy()
            self.basis_data['Urlaube']=self.basis_data['Urlaube'].append(data_hol,ignore_index=True)
        self.basis_data['Urlaube']=self.basis_data['Urlaube'].sort_values(['time1'],ascending=False)
        if len(self.basis_data['Urlaube'].index)>0:
            self.folder_res['Urlaube']=self.folder_raw+'Ergebnisse/Urlaubsauswertung'
            self.plotting_list['Urlaube']=['c','m','v']
        else:
            del self.basis_data['Urlaube']

            
    def month_cat_maker(self):
        
        ##categorize data and sort ascending. Same goes for monthly data
        folder_list=list(self.folder_res.keys())
        for item in range(0,len(folder_list)):
            basis_data_subset=self.basis_data[folder_list[item]].copy()

            summen_rest=abs(basis_data_subset[basis_data_subset['cat'].isin(['Aktien-\ngeschäfte','Gehalt','ETFS / Wert-\npapiersparen','Bucheinkünfte','Sonstige\nEinkünfte','Einzahlungen','Miete'])==False]['val'].sum())
            self.summe_rest[folder_list[item]]=summen_rest

            self.month_data[folder_list[item]]=basis_data_subset.groupby('month',sort=False)['val'].sum().reset_index() ##get monthly overview
            self.month_data[folder_list[item]]=self.month_data[folder_list[item]].sort_values(['val'],ascending=False) ##sort monthly data

            categorizer=basis_data_subset.groupby('cat')['val'].sum().reset_index()
                        
            hilfs=pd.DataFrame([['Gesamtsaldo\nder Periode', sum(basis_data_subset['val'])]],columns=list(categorizer.columns))
            categorizer=categorizer.sort_values(['val'],ascending=False).reset_index(drop=True)
            cat_data=categorizer.loc[(categorizer['val']>0)&(categorizer['cat'].isin(['Gehalt','Bucheinkünfte','Sonstige\nEinkünfte','Einzahlungen']))]

            if len(cat_data.index)>0:
                cost_data=categorizer[cat_data.index[-1]+1:].copy()
            else:
                cost_data=categorizer[0:].copy()

            cost_data['val']=cost_data['val'].abs()
            cost_data=cost_data.sort_values(['val'],ascending=False)
            cat_data=cat_data.append(cost_data,ignore_index=True)
            cat_data=cat_data.append(hilfs,ignore_index=True)
            cat_data['val_month']=cat_data['val']/(self.month_data[folder_list[item]]['month'].nunique())
            self.cat_data[folder_list[item]]=cat_data
    
    
    def makefolder_excel(self):
        ## Create folders and export data into excel with several subsheets
        folder_list=list(self.folder_res.keys())
        for item in range(0,len(folder_list)):
            result_dir=self.folder_res[folder_list[item]]
            try:
                if not os.path.exists(result_dir):
                    os.makedirs(result_dir)
            except OSError:
                print ('Error: Creating directory. ' +  result_dir)
            writer_excel = pd.ExcelWriter(result_dir+'/Auswertungstabelle.xlsx', engine='xlsxwriter')

            if folder_list[item] in self.raw_data:
                self.raw_data[folder_list[item]].to_excel(writer_excel,sheet_name='Rohdaten',index=False,header=self.raw_data_header[folder_list[item]])
            else:#no action needed
                pass

            self.basis_data[folder_list[item]].to_excel(writer_excel,sheet_name='Aufbereitete Daten',index=False,header=["Buchungstag","Wertstellung (Valuta)","Vorgang","Buchungsinformationen","Beträge in EUR","Monat","Kategorie"])
            
            if folder_list[item] in self.cat_data:
                self.cat_data[folder_list[item]].to_excel(writer_excel,sheet_name='Übersicht nach Kategorie.xlsx',index=False,header=["Kategorie","Beträge in EUR","Beträge pro Monat"])
            else:#no action needed
                pass

            writer_excel.save()
