
'''This file is needed to preprocess the submitted data to fit the needed format for the plotting functions. Cost data is separated with regards to investment costs and total costs.
Pie relative portion charts are created for cost components >2%, and shown separately according to investment and holiday costs with regards to total costs.'''

from Base_Functions import plotters
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt 

class Plotters:

    def __init__(self,accounts_data):

        self.plot_elements=list(accounts_data.plotting_list.keys())
        self.plotting_choice=accounts_data.plotting_list
        self.basis_data=accounts_data.basis_data
        self.month_data=accounts_data.month_data
        self.summe_rest=accounts_data.summe_rest
        self.cat_data=accounts_data.cat_data
        self.top3={}
        self.top3_adj={}
        self.plotinfo_costs={}
        self.plotinfo_piechart={}
        self.folder_res=accounts_data.folder_res

        
    ##Prepare Plot-Data:
    def makeplotdata(self):
        
        ##Shuffle through items:
        for item in range(0,len(self.plot_elements)):
            element_name=self.plot_elements[item]
          
                       
            ##Cost plot Main Data
            cost_total=self.cat_data[element_name][self.cat_data[element_name]['cat'].isin(['Einzahlungen','Sonstige\nEinkünfte','Lohn','Bucheinkünfte','Gesamtsaldo\nder Periode'])==False].reset_index(drop=True)
            self.plotinfo_costs[element_name]=[(cost_total,f'Detaillierte Gesamtübersicht Kostenkategorien für "{element_name}"',self.folder_res[element_name]+'/Komplettübersicht Kostenkategorien.jpg')]
                
            ##Prepare TOP3-Plots:
            if 'o' in self.plotting_choice[element_name]:
                
                ##Basic TOP3 (define length of top3-list)
                def length_top3():
                    for i in range(1,len(self.cat_data[element_name])-1):
                        if self.cat_data[element_name]['val'][i]>self.cat_data[element_name]['val'][i-1]:
                            return i-1
                        else:
                            pass
                    else:
                        return 0
                index_val=length_top3()
                self.top3[element_name]=self.cat_data[element_name].iloc[:(4+index_val)].reset_index(drop=True)
                        

                
                ##Adjusted TOP3 without invest
                top3_adj=self.cat_data[element_name][self.cat_data[element_name]['cat'].isin(['Miete','Gesamtsaldo\nder Periode'])]
                lohn_inv=self.basis_data[element_name][self.basis_data[element_name]['cat'].isin(['Einzahlungen','Sonstige\nEinkünfte','Gehalt','Bucheinkünfte'])]['val'].sum()-abs(self.basis_data[element_name][self.basis_data[element_name]['cat'].isin(['Aktien-\ngeschäfte','ETFS / Wert-\npapiersparen'])]['val'].sum())
                top3_adj=top3_adj.append(pd.DataFrame([['Restliche Kosten-\npositionen', self.summe_rest[element_name],self.summe_rest[element_name]/self.month_data[element_name]['month'].nunique()]],columns=list(top3_adj.columns)),ignore_index=True)
                top3_adj=top3_adj.append(pd.DataFrame([['Einkünfte inkl.\nInvestergebnis', lohn_inv,lohn_inv/self.month_data[element_name]['month'].nunique()]],columns=list(top3_adj.columns)),ignore_index=True)
                top3_adj=top3_adj.sort_values(['val'],ascending=False).reset_index(drop=True)
                self.top3_adj[element_name]=top3_adj
            
            else:#no action needed    
                pass
            ##Pie Chart holiday-->every account with'a' or 't'

            cost_hol=cost_total.loc[cost_total['cat'].str.contains('Urlaub')]
            cost_nothol=cost_total.loc[cost_total['cat'].str.contains('Urlaub')==False]
            
            ##Adjust total cost with holidays grouped together
            cost_total_adj=cost_nothol.append(pd.DataFrame([['Urlaube gesamt',sum(cost_hol['val']),sum(cost_hol['val_month'])]], columns=list(cost_nothol.columns)),ignore_index=True)
            cost_total_adj=cost_total_adj.sort_values('val',ascending=False).reset_index(drop=True)

            
            ##Pie costs without holidays (right side)
            cost_intermediate_hol=cost_nothol.assign(ppt=(cost_nothol['val']*100/cost_nothol['val'].sum()).round(2))
            cost_intermediate_hol.drop('val_month',axis=1,inplace=True)
            cost_pie_nothol=cost_intermediate_hol.loc[cost_intermediate_hol['ppt']>=2.0]
            cost_pie_nothol=cost_pie_nothol.append(pd.DataFrame([['Restliche mit <2%',sum(cost_intermediate_hol.loc[cost_intermediate_hol['ppt']<2.0]['val']),sum(cost_intermediate_hol.loc[cost_intermediate_hol['ppt']<2.0]['ppt'])]],columns=list(cost_intermediate_hol.columns)),ignore_index=True)

            ##Pie costs with holidays together (left side)
            cost_intermediate2_hol=cost_total_adj.assign(ppt=(cost_total_adj['val']*100/cost_total_adj['val'].sum()).round(2))
            cost_intermediate2_hol.drop('val_month',axis=1,inplace=True)
            cost_pie_total_holadj=cost_intermediate2_hol.loc[cost_intermediate2_hol['ppt']>=2.0]
            cost_pie_total_holadj=cost_pie_total_holadj.append(pd.DataFrame([['Restliche mit <2%',sum(cost_intermediate2_hol.loc[cost_intermediate2_hol['ppt']<2.0]['val']),sum(cost_intermediate2_hol.loc[cost_intermediate2_hol['ppt']<2.0]['ppt'])]],columns=list(cost_intermediate2_hol.columns)),ignore_index=True)
            
            data_pie_hol=(cost_pie_total_holadj,cost_pie_nothol)
            plotinfo_pieplot_hol=(f'Tortendiagramm Kostenkategorien >2% Anteil für "{element_name}"','Anteilsübersicht mit Urlauben zusammengefasst','Anteilsübersicht ohne Urlaube',self.folder_res[element_name]+'/Tortendiagramm Kostenanteile_Urlaub.jpg')
            self.plotinfo_piechart[element_name]=[(data_pie_hol,plotinfo_pieplot_hol)]
            
            ##Adjust Cost & pie plots for invest
            if len(self.cat_data[element_name][self.cat_data[element_name]['cat'].isin(['Aktien-\ngeschäfte','ETFS / Wert-\npapiersparen'])].index) > 0:
                cost_notinv=cost_total[cost_total['cat'].isin(['Aktien-\ngeschäfte','ETFS / Wert-\npapiersparen'])==False].reset_index(drop=True)
                self.plotinfo_costs[element_name].append((cost_notinv,f'Detaillierte Übersicht Kostenkategorien ohne Investkosten für "{element_name}"',self.folder_res[element_name]+'/Kostenübersicht ohne Invest.jpg'))
            
                ##Get second pie plot, if invest is existing
                
                ##Pie costs without invest (right side)
                cost_notinv_adj=cost_total_adj[cost_total_adj['cat'].isin(['Aktien-\ngeschäfte','ETFS / Wert-\npapiersparen'])==False].reset_index(drop=True) ##Get costs without invest with holidays together
                cost_intermediate_inv=cost_notinv_adj.assign(ppt=(cost_notinv_adj['val']*100/cost_notinv_adj['val'].sum()).round(2))
                cost_intermediate_inv.drop('val_month',axis=1,inplace=True)
                cost_pie_notinv=cost_intermediate_inv.loc[cost_intermediate_inv['ppt']>=2.0]
                cost_pie_notinv=cost_pie_notinv.append(pd.DataFrame([['Restliche mit <2%',sum(cost_intermediate_inv.loc[cost_intermediate_inv['ppt']<2.0]['val']),sum(cost_intermediate_inv.loc[cost_intermediate_inv['ppt']<2.0]['ppt'])]],columns=list(cost_intermediate_inv.columns)),ignore_index=True)

                ##Pie costs with invest (left side)
                cost_intermediate2_inv=cost_total_adj.assign(ppt=(cost_total_adj['val']*100/cost_total_adj['val'].sum()).round(2))
                cost_intermediate2_inv.drop('val_month',axis=1,inplace=True)
                cost_pie_total_invadj=cost_intermediate2_inv.loc[cost_intermediate2_inv['ppt']>=2.0]
                cost_pie_total_invadj=cost_pie_total_invadj.append(pd.DataFrame([['Restliche mit <2%',sum(cost_intermediate2_inv.loc[cost_intermediate2_inv['ppt']<2.0]['val']),sum(cost_intermediate2_inv.loc[cost_intermediate2_inv['ppt']<2.0]['ppt'])]],columns=list(cost_intermediate2_inv.columns)),ignore_index=True)
                
                data_pie_inv=(cost_pie_total_invadj,cost_pie_notinv)
                plotinfo_pieplot_inv=(f'Tortendiagramm Kostenkategorien >2% Anteil für "{element_name}"','Anteilsübersicht mit Invest','Anteilsübersicht ohne Invest',self.folder_res[element_name]+'/Tortendiagramm Kostenanteile_Invest.jpg')
                self.plotinfo_piechart[element_name].append((data_pie_inv,plotinfo_pieplot_inv))                             

            else:#no action needed
                pass

    def plotdata(self):
        
        for item in range(0,len(self.plot_elements)):
            element_name=self.plot_elements[item]
            for choice in range(0,len(self.plotting_choice[element_name])):
                
                plot_selection=self.plotting_choice[element_name][choice]
                
                ##Plot selection
                #Boxplot
                if plot_selection=='b':
                    boxplottitle=f'Umsatzübersicht (Boxplot) nach Kategorie für "{element_name}"'
                    printname_box=self.folder_res[element_name]+'/Boxplot_Übersicht.jpg'
                    plotters.boxplotter(self.basis_data[element_name],self.month_data[element_name],boxplottitle,printname_box)

                #Violinplot
                elif plot_selection=='v':
                    
                    violintitle=f'Detaillierte Umsatzübersicht (Violinplot) nach Kategorie für "{element_name}"'
                    printname_vio=self.folder_res[element_name]+'/Violinplot_Übersicht.jpg'
                    plotters.violinplotter(self.basis_data[element_name],self.month_data[element_name],violintitle,printname_vio)

                #Monthplot
                elif plot_selection=='m':
                    monthtitle=f'Monatsaufstellung für "{element_name}"'
                    printname_month=self.folder_res[element_name]+'/Monatsaufstellung.jpg'
                    plotters.monthplotter(self.month_data[element_name],monthtitle,printname_month)

                #TOP3-Plots
                elif plot_selection=='o':
                    plotinfo_overview=(f'Überblicksübersicht Kosten und Einkünfte für "{element_name}"','Einkünfte und TOP-Kostenblöcke','Einkünfte ohne Invest & Restliche Kosten',self.folder_res[element_name]+'/TOP3-Überblick.jpg')
                    plotters.overviewplot(self.top3[element_name],self.top3_adj[element_name],self.month_data[element_name],plotinfo_overview)

                #Categorical cost plot
                elif plot_selection=='c':
                    for cost_entry in range(0,len(self.plotinfo_costs[element_name])):
                        cost_plotinfo=self.plotinfo_costs[element_name][cost_entry]                        
                        plotters.costplotter(cost_plotinfo,self.month_data[element_name])
                        
                #Pie chart
                elif plot_selection=='t':
                    for pie_entry in range(0,len(self.plotinfo_piechart[element_name])):
                        pie_plotinfo=self.plotinfo_piechart[element_name][pie_entry]  
                        plotters.pieplotter(pie_plotinfo,self.month_data[element_name])
                #Alltogether
                else: 
                    #Boxplot
                    boxplottitle=f'Umsatzübersicht (Boxplot) nach Kategorie für "{element_name}"'
                    printname_box=self.folder_res[element_name]+'/Boxplot_Übersicht.jpg'
                    plotters.boxplotter(self.basis_data[element_name],self.month_data[element_name],boxplottitle,printname_box)

                    #Violinplot
                    violintitle=f'Detaillierte Umsatzübersicht (Violinplot) nach Kategorie für "{element_name}"'
                    printname_vio=self.folder_res[element_name]+'/Violinplot_Übersicht.jpg'
                    plotters.violinplotter(self.basis_data[element_name],self.month_data[element_name],violintitle,printname_vio)

                    #Month plot
                    monthtitle=f'Monatsaufstellung für "{element_name}"'
                    printname_month=self.folder_res[element_name]+'/Monatsaufstellung.jpg'
                    plotters.monthplotter(self.month_data[element_name],monthtitle,printname_month)
                    
                    #Categorical cost plot
                    for cost_entry in range(0,len(self.plotinfo_costs[element_name])):
                        cost_plotinfo=self.plotinfo_costs[element_name][cost_entry]                        
                        plotters.costplotter(cost_plotinfo,self.month_data[element_name])
                        
                    #Pie chart
                    for pie_entry in range(0,len(self.plotinfo_piechart[element_name])):
                        pie_plotinfo=self.plotinfo_piechart[element_name][pie_entry]  
                        plotters.pieplotter(pie_plotinfo,self.month_data[element_name])
