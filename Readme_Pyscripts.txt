Readme Account_Analysis:

Mit diesem Python-Skript k�nnen die eigenen Kontodaten ausgewertet werden, wobei am Ende der Auswertung eine aufbereitete Excel-Datei steht sowie mehrere Plots zur Auswertung im jpg-Format ausgegeben werden. Das Skript ist auf einer Linux-Python ausgerichtet, eine Windows-Exe kann auf Nachfrage ebenfalls bereitgestellt werden.
Basis s�mtlicher Auswertungen ist die Zuordnung einzelner Buchungen zu Kostengrupper/-kategorien. Daf�r wird die Excel-Zuordnungstabelle genutzt. Wichtig ist, dass der Suchtext klein und zusammengeschrieben wird, Satzzeichen (./?!-_:;,) bleiben erhalten.
Das Skript nimmt den Buchungstext und entfernt Leerzeichen und Gro�buchstaben zur eindeutigen Zuordenbarkeit. Kategorien (rechte Spalte) k�nnen mehr oder weniger frei gew�hlt werden, allerdings werden "Lohn","Einzahlungen" und "Sonstige Eink�nfte" f�r bestimmte Zuordnungen ben�tigt. Insofern sollten diese Namen nicht ver�ndert werden. Gleiches gilt f�r "Miete" und "Aktiengesch�fte","ETFS / Wertpapiersparen". Sofern Vorg�nge in diesen Bereichen erfolgt sind, sollten sie auch diesen Kategorien zugeordnet werden, um entsprechende Plots zu erhalten. Es wird empfohlen einmal alle Buchungstexte durchzusehen, um Wiederholungen zu erkennen und die Zuordnungstabelle entsprechend anzupassen. So k�nnen solche Buchungen auch bei zuk�nftigen Analysedurchg�ngen erkannt und entsprechend den Kategorien zugeschl�sselt werden.
Alle Buchungen ohne entsprechende Schl�sselw�rter werden unter "Sonstiges" kategorisiert.

Wichtig vor der Auswertung ist, auf die Frage des Datenordners den genauen Dateipfad des Ordners anzugeben, in dem sich die *.csv-Dateien befinden (Linux: /.../.../). Auf die Frage des 
Dateinamens ist der komplette Dateiname mit Dateiendung einzugeben, sonst bricht das Skript ab.

Ausgef�hrt wird nur das Hauptskript "Analyser_Mainscript.py". Darin erfolgen einige nachfragen, was ausgewertet werden soll. Es kann eine csv-Datei eingelesen und aufbereitet werden oder eine Excel-Datei, sofern sie die in diesem Skript definierte Formatierung aufweist. Das Einlesen von Excel-Dateien dient vor allem dazu bereits aufbereitete csv-Dateien, die manuell nachkategorisiert wurden, erneut in Diagrammen auszugeben.
Beim Import von csv-Dateien ist der Kontotyp einzugeben. Aktuell werden folgende Umsatz-Export-csv-Dateien unterst�tzt:
1) Comdirect Girokonto und Kreditkarten-Daten (m�ssen separat exportiert und eingelesen werden)
2) DKB Girokonto und Kreditkarten-Daten (m�ssen separat exportiert und eingelesen werden)
3) Triodos-/MLP-Girokontodaten.

Ausgegeben werden aktuell ein Boxplot & Violinplot (verbesserte Darstellung bei nahelienden Datenpunkten) sowie eine �berblicks�bersicht, eine Monats�bersicht, eine Kostenkategorie�bersicht und ein Tortendiagramm mit den Kategorieanteilen >2%.

Bitte bedenken: Das Skript ist Work in Progress, also manches l�uft noch nicht so toll oder sieht nicht so toll aus.

Viel Spa� damit!




